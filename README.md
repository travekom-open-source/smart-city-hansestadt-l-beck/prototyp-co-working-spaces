# Prototyp Co-Working Spaces und Zusammenarbeitsräume

Zur Verwaltung von Co-Working Spaces und Lernräumen in der Hansestadt Lübeck soll eine Datenbank geschaffen werden. Vorgaben waren hier keine vorhanden, so dass mit diesen Prototypen eine erste Diskussionsgrundlage geschaffen wurde, um die Anforderungen im Rahmen eines Umsetzungsprojektes weiter zu schärfen.

Die Datenbank für Co-Working Spaces und Lernräume besteht aus insgesamt vier einzelnen Seiten im Corporate Design der Hansestadt Lübeck:

-	Verwaltungsfrontend
-	Buchungsseite
-	Öffentliche Seite zum Suchen der Co-Working Spaces und Lernräume
-	Öffentliche Detailseite eines der Co-Working Spaces oder Lernraumes

Elemente sind anhand der Berechtigung nutzbar. Ein Public User kann demnach nicht auf das Verwaltungsfrontend oder die Buchungsseite zugreifen.

Zur Persistenz der Daten wird im Prototyp die integrierte Datenbank der Application Builder Platform verwendet. Bei eine Produktivstellung muss hier eine entsprechende robuste Datenbanktechnologie verwendet werden

<img src="https://gitlab.com/travekom-open-source/smart-city-hansestadt-l-beck/prototyp-co-working-spaces/-/raw/main/Prototyp_Co-Working-Spaces.png">

## Installation
Zur Installation der App kann Budibase in der Cloud- oder onPremise Variante genutzt werden. Beim Erstellen einer neuen App kann dann das hier hinterlegte Archiv importiert werden.

## Nutzung
Die App ist nicht für eine produktive Nutzung gedacht, sondern wurde im Rahmen einen Domain Prototypings erstellt um als Grundlage für weitere Projekte zu dienen. Das per iFrame eingebundene Widget von wheelmap.org muss bei produktiver Nutzung gegen eine jährliche Gebühr beim Anbieter freigeschaltet werden.

## Support
Anfragen zum Projekt an team_trk_udp@travekom.de

## Contributing
Veränderungen am Sourcecode können in Rahmen von Feature-Branches mit anschließendem MergeRequest vorgenommen werden. Der MergeReqeust wird von einem Maintainer der reviewed.

## License
Das Projekt steht unter der Apache 2.0 Lizenz
